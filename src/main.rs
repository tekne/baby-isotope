use std::time::Instant;

use egg::*;
use fxhash::FxHashSet;
use rustyline::error::ReadlineError;
use rustyline::Editor;

#[derive(Default)]
struct IsoAnalysis;

#[derive(Debug, Clone, Eq, PartialEq, Default)]
struct IsoData {
    /// This set's free variables
    fv: FxHashSet<String>,
    /// If this is a variable
    is_var: bool,
    /// If this is a simple constant
    is_cv: bool,
    cv: Option<BabyIso>,
    /// Whether this value *cannot* cause UB, including transitively
    no_ub: bool,
}

impl IsoData {
    fn join(&self, other: &IsoData) -> IsoData {
        let (mut fv, i) = if self.fv.len() > other.fv.len() {
            (self.fv.clone(), &other.fv)
        } else {
            (other.fv.clone(), &self.fv)
        };
        fv.extend(i.iter().cloned());
        IsoData {
            fv,
            is_var: false,
            cv: None,
            no_ub: self.no_ub | other.no_ub,
            is_cv: self.is_cv & other.is_cv,
        }
    }

    fn joined(mut self, other: &IsoData) -> IsoData {
        self.fv.extend(other.fv.iter().cloned());
        self.is_var = false;
        self
    }
}

impl Analysis<BabyIso> for IsoAnalysis {
    type Data = IsoData;

    fn make(egraph: &EGraph<BabyIso, Self>, enode: &BabyIso) -> Self::Data {
        let f = |i: &Id| &egraph[*i].data;
        match enode {
            BabyIso::Symbol(s) => IsoData {
                fv: FxHashSet::from_iter([s.as_str().to_string()]),
                is_var: true,
                is_cv: false,
                cv: None,
                no_ub: true,
            },
            BabyIso::Let([_, e, e_]) => f(e).join(f(e_)),
            BabyIso::Letp([_, _, e, e_]) => f(e).join(f(e_)),
            BabyIso::App([l, r]) => f(l).join(f(r)),
            //TODO: optimize
            BabyIso::Switch(s) => s.iter().map(f).fold(IsoData::default(), IsoData::joined),
            b => IsoData {
                fv: Default::default(),
                is_var: false,
                is_cv: b.is_cv(),
                cv: b.to_const(),
                no_ub: b.no_ub(),
            },
        }
    }

    fn merge(&mut self, a: &mut Self::Data, mut b: Self::Data) -> DidMerge {
        //TODO: clean up cv check?
        let cva = a.cv.clone();
        let cvb = b.cv.clone();
        let a_fv = a.fv.len();
        let b_fv = b.fv.len();
        let a_is_var = a.is_var;
        let b_is_var = b.is_var;
        let a_is_cv = a.is_cv;
        let b_is_cv = b.is_cv;
        let a_no_ub = a.no_ub;
        let b_no_ub = b.no_ub;
        let swapped = a.fv.len() > b.fv.len();
        if swapped {
            std::mem::swap(a, &mut b);
        }
        let l = a;
        let r = b;
        l.fv.retain(|s| r.fv.contains(s));
        l.is_var |= r.is_var;
        if l.cv.is_none() {
            if cva.is_some() {
                l.cv = cva.clone();
            } else if cvb.is_some() {
                l.cv = cvb.clone();
            }
        }
        l.no_ub |= r.no_ub;
        l.is_cv |= r.is_cv;
        let l_fv = l.fv.len();
        let cvad = cva != l.cv;
        let cvbd = cvb != l.cv;
        DidMerge(
            l_fv != a_fv
                || l.is_var != a_is_var
                || cvad
                || l.no_ub != a_no_ub
                || l.is_cv != a_is_cv,
            l_fv != b_fv
                || l.is_var != b_is_var
                || cvbd
                || l.no_ub != b_no_ub
                || l.is_cv != b_is_cv,
        )
    }
}

fn var(s: &str) -> Var {
    s.parse().unwrap()
}

fn pat(s: &str) -> PatternAst<BabyIso> {
    s.parse().unwrap()
}

fn does_not_contain<const N: usize>(
    variables: [Var; N],
    value: Var,
) -> impl Condition<BabyIso, IsoAnalysis> {
    move |e: &mut EGraph<BabyIso, IsoAnalysis>, _, subst: &Subst| {
        //TODO: is it safe to use min_fv here?
        let val = subst[value];
        for variable in variables {
            let var = subst[variable];
            if !e[var].data.fv.iter().all(|v| !e[val].data.fv.contains(v)) {
                return false;
            }
        }
        true
    }
}

fn arith_refine<const N: usize>(values: [Var; N]) -> impl Condition<BabyIso, IsoAnalysis> {
    move |e: &mut EGraph<BabyIso, IsoAnalysis>, _, subst: &Subst| {
        values
            .into_iter()
            .all(|value| e[subst[value]].data.no_ub && !e[subst[value]].data.is_cv)
    }
}

define_language! {
    enum BabyIso {
        // Basic
        "let" = Let([Id; 3]),
        "letp" = Letp([Id; 4]),
        "app" = App([Id; 2]),

        // Basic blocks
        //"jump" = Jump,
        //"bb" = Block(Box<[Id]>),

        // Functions
        //"fun" = Fun(Box<[Id]>),

        // Control flow
        "switch" = Switch([Id; 2]),
        "br" = Branch([Id; 4]),
        "ub" = UB,
        "abort" = AB,

        // Memory
        "!" = Split,
        "~" = Join,
        "." = Emp,
        "malloc" = Malloc,
        "free" = Free,
        "load" = Load,
        "store" = Store,
        "loadable" = Loadable,
        "storable" = Storable,
        "asserted" = Asserted,

        // Arithmetic
        "+" = Add,
        "-" = Sub,
        "neg" = Neg,
        "/" = Div,
        "%" = Mod,
        "*" = Mul,
        "<<" = Shl,
        ">>" = Shr,
        "<=" = Le,
        ">=" = Ge,
        "<" = Lt,
        ">" = Gt,
        "&" = And,
        "|" = Or,
        "^" = Xor,
        "not" = Not,
        Num(i64),

        // Logic
        Bool(bool),

        // Variables
        Symbol(Symbol),
        Name(String),
    }
}

impl BabyIso {
    fn op_cost(&self) -> f64 {
        match self {
            BabyIso::Switch(_) => 1.0,
            BabyIso::Split => 0.2,
            BabyIso::Join => 0.2,
            BabyIso::Malloc => 100.0,
            BabyIso::Free => 100.0,
            BabyIso::Load => 16.0,
            BabyIso::Store => 16.0,
            BabyIso::Add => 2.0,
            BabyIso::Sub => 2.0,
            BabyIso::Neg => 2.0,
            BabyIso::Div => 8.0,
            BabyIso::Mod => 8.0,
            BabyIso::Mul => 4.0,
            BabyIso::Shl => 1.0,
            BabyIso::Shr => 1.0,
            BabyIso::And => 1.0,
            BabyIso::Or => 1.0,
            BabyIso::Xor => 1.0,
            BabyIso::Not => 1.0,
            BabyIso::Le => 2.0,
            BabyIso::Lt => 2.0,
            BabyIso::Ge => 2.0,
            BabyIso::Gt => 2.0,
            BabyIso::Symbol(_) => 1.0,
            _ => 0.0,
        }
    }
}

struct IsoCost;

impl CostFunction<BabyIso> for IsoCost {
    type Cost = f64;

    fn cost<C>(&mut self, enode: &BabyIso, mut costs: C) -> Self::Cost
    where
        C: FnMut(Id) -> Self::Cost,
    {
        match enode {
            BabyIso::Branch([_lo, _hi, v, b]) => costs(*v).max(costs(*b)),
            enode => enode.fold(enode.op_cost(), |sum, id| sum + costs(id)),
        }
    }
}

impl BabyIso {
    pub fn to_const(&self) -> Option<BabyIso> {
        if matches!(
            self,
            BabyIso::Num(_) | BabyIso::Bool(_) | BabyIso::Emp | BabyIso::AB | BabyIso::UB
        ) {
            Some(self.clone())
        } else {
            None
        }
    }

    pub fn no_ub(&self) -> bool {
        matches!(
            self,
            BabyIso::Num(_)
                | BabyIso::Bool(_)
                | BabyIso::Emp
                | BabyIso::AB
                | BabyIso::Add
                | BabyIso::Sub
                | BabyIso::Div
                | BabyIso::Mod
                | BabyIso::Mul
                | BabyIso::Shl
                | BabyIso::Shr
                | BabyIso::And
                | BabyIso::Or
                | BabyIso::Xor
        )
    }

    pub fn is_cv(&self) -> bool {
        matches!(
            self,
            BabyIso::Num(_)
                | BabyIso::Bool(_)
                | BabyIso::Emp
                | BabyIso::UB
                | BabyIso::AB
                | BabyIso::Add
                | BabyIso::Sub
                | BabyIso::Div
                | BabyIso::Mod
                | BabyIso::Mul
                | BabyIso::Shl
                | BabyIso::Shr
                | BabyIso::And
                | BabyIso::Or
                | BabyIso::Xor
        )
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct AddEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for AddEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a.wrapping_add(*b)));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "add-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct SubEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for SubEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a.wrapping_sub(*b)));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "sub-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct MulEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    shl_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for MulEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a.wrapping_mul(*b)));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "mul-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            (_, Some(BabyIso::Num(n))) if *n > 0 && (*n as u64).is_power_of_two() => {
                let n = *n;
                let logn = egraph.add(BabyIso::Num((n as u64).trailing_zeros() as i64));
                let mut subst = subst.clone();
                subst.insert(self.c, logn);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.shl_pat, &subst, "mul-shl")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct DivEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    shr_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for DivEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a.wrapping_div(*b)));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "div-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            (_, Some(BabyIso::Num(n))) if *n > 0 && (*n as u64).is_power_of_two() => {
                let n = *n;
                let logn = egraph.add(BabyIso::Num((n as u64).trailing_zeros() as i64));
                let mut subst = subst.clone();
                subst.insert(self.c, logn);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.shr_pat, &subst, "div-shr")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct ShlEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for ShlEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a.wrapping_shl(*b as u32)));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "shl-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct ShrEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for ShrEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a.wrapping_shl(*b as u32)));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "shr-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct AndEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for AndEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a & b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) = egraph.union_instantiations(
                    &self.from_pat,
                    &self.to_pat,
                    &subst,
                    "and-eval-int",
                ) {
                    vec![id]
                } else {
                    vec![]
                }
            }
            (Some(BabyIso::Bool(a)), Some(BabyIso::Bool(b))) => {
                let c = egraph.add(BabyIso::Bool(a & b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) = egraph.union_instantiations(
                    &self.from_pat,
                    &self.to_pat,
                    &subst,
                    "and-eval-bool",
                ) {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct OrEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for OrEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a | b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "or-eval-int")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            (Some(BabyIso::Bool(a)), Some(BabyIso::Bool(b))) => {
                let c = egraph.add(BabyIso::Bool(a | b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) = egraph.union_instantiations(
                    &self.from_pat,
                    &self.to_pat,
                    &subst,
                    "or-eval-bool",
                ) {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct XorEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for XorEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Num(a ^ b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) = egraph.union_instantiations(
                    &self.from_pat,
                    &self.to_pat,
                    &subst,
                    "xor-eval-int",
                ) {
                    vec![id]
                } else {
                    vec![]
                }
            }
            (Some(BabyIso::Bool(a)), Some(BabyIso::Bool(b))) => {
                let c = egraph.add(BabyIso::Bool(a ^ b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) = egraph.union_instantiations(
                    &self.from_pat,
                    &self.to_pat,
                    &subst,
                    "xor-eval-bool",
                ) {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct LeEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for LeEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Bool(a <= b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "le-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct LtEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for LtEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Bool(a < b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "le-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct GeEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for GeEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Bool(a >= b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "ge-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct GtEval {
    from_pat: PatternAst<BabyIso>,
    to_pat: PatternAst<BabyIso>,
    a: Var,
    b: Var,
    c: Var,
}

impl Applier<BabyIso, IsoAnalysis> for GtEval {
    fn apply_one(
        &self,
        egraph: &mut EGraph<BabyIso, IsoAnalysis>,
        _eclass: Id,
        subst: &Subst,
        _searcher_ast: Option<&PatternAst<BabyIso>>,
        _rule_name: Symbol,
    ) -> Vec<Id> {
        let a: Id = subst[self.a];
        let b: Id = subst[self.b];
        match (&egraph[a].data.cv, &egraph[b].data.cv) {
            (Some(BabyIso::Num(a)), Some(BabyIso::Num(b))) => {
                let c = egraph.add(BabyIso::Bool(a > b));
                let mut subst = subst.clone();
                subst.insert(self.c, c);
                if let (id, true) =
                    egraph.union_instantiations(&self.from_pat, &self.to_pat, &subst, "gt-eval")
                {
                    vec![id]
                } else {
                    vec![]
                }
            }
            _ => {
                vec![]
            }
        }
    }
}

fn make_rules() -> Vec<Rewrite<BabyIso, IsoAnalysis>> {
    vec![
        // Substitution
        // Basic substitution
        rewrite!("let-const"; "(let ?x ?v ?e)" => "?e" if does_not_contain([var("?x")], var("?e"))),
        rewrite!("let-subst"; "(let ?x ?v ?x)" => "?v"),
        rewrite!("let-let"; "(let ?x ?xv (let ?y ?yv ?e))" => "(let ?y (let ?x ?xv ?yv) (let ?x ?xv ?e))"),
        rewrite!("let-letp"; "(let ?x ?xv (letp ?y ?z ?yzv ?e))" => "(letp ?y ?z (let ?x ?xv ?yzv) (let ?x ?xv ?e))"),
        rewrite!("let-app"; "(let ?x ?v (app ?l ?r))" => "(app (let ?x ?v ?l) (let ?x ?v ?r))"),
        rewrite!("let-switch"; "(let ?x ?v (switch ?d ?b))" => "(switch (let ?x ?v ?d) (let ?x ?v ?b))"),
        rewrite!("let-branch"; "(let ?x ?v (br ?lo ?hi ?l ?n))" => "(br ?lo ?hi (let ?x ?v ?l) (let ?x ?v ?n))"),
        // Binary, linear substitution
        rewrite!("letp-eta"; "(letp ?x ?y ?v (app (app pair ?x) ?y))" => "?v"),
        rewrite!("letp-pair"; "(letp ?x ?y (app (app pair ?l) ?r) ?e)" => "(let ?x ?l (let ?y ?r ?e))"),
        rewrite!("letp-const"; "(letp ?x ?y ?v ?e)" => "?e" if does_not_contain([var("?x"), var("?y")], var("?e"))),
        rewrite!("letp-left"; "(letp ?x ?y ?v (app ?l ?r))" => "(app (letp ?x ?y ?v ?l) ?r)" if does_not_contain([var("?x"), var("?y")], var("?r"))),
        rewrite!("letp-right"; "(letp ?x ?y ?v (app ?l ?r))" => "(app ?l (letp ?x ?y ?v ?r))" if does_not_contain([var("?x"), var("?y")], var("?l"))),
        rewrite!("letp-disc"; "(letp ?x ?y ?v (switch ?d ?b))" => "(switch (letp ?x ?y ?v ?d) ?b)" if does_not_contain([var("?x"), var("?y")], var("?b"))),
        rewrite!("letp-switch"; "(letp ?x ?y ?v (switch ?d ?b))" => "(switch ?d (letp ?x ?y ?v ?b))" if does_not_contain([var("?x"), var("?y")], var("?d"))),
        rewrite!("letp-branch"; "(letp ?x ?y ?v (br ?lo ?hi ?l ?n))" => "(br ?lo ?hi (letp ?x ?y ?v ?l) (letp ?x ?y ?v ?n))"),
        // Basic assertions
        rewrite!("assert-true"; "(app (app assert true) ?s)" => "?s"),
        rewrite!("assert-false"; "(app (app assert false) ?s)" => "false"),
        rewrite!("assert-merge"; "(app (app assert ?p) (app (app assert ?q) ?s))" => "(app (app assert (app (app & ?p) ?q)) ?s)"),
        // Control flow
        // Basic UB
        rewrite!("app-left-ub"; "(app ub ?x)" => "ub"),
        rewrite!("app-right-ub"; "(app ?x ub)" => "ub"),
        rewrite!("ub-disc"; "(switch ub ?b)" => "ub"),
        rewrite!("ub-switch"; "(switch ?d ub)" => "ub"),
        rewrite!("ub-branch"; "(br ?lo ?hi ub ?b)" => "?b"),
        // Basic memory
        rewrite!("comm-split"; "(letp ?x ?y (app ! ?s) ?v)" => "(letp ?y ?x (app ! ?s) ?v)"),
        rewrite!("comm-join"; "(app (app ~ ?x) ?y)" => "(app (app ~ ?y) ?x)"),
        rewrite!("join-emp"; "(app (app ~ ?x) .)" => "?x"),
        rewrite!("split-emp"; "(app ! .)" => "(app (app pair .) .)"),
        rewrite!("assert-join-left"; "(app (app ~ (app (app assert ?p) ?x)) ?y)" => "(app (app assert ?p) (app (app ~ ?x) ?y))"),
        rewrite!("assert-join-right"; "(app (app ~ ?x) (app (app assert ?p) ?y))" => "(app (app assert ?p) (app (app ~ ?x) ?y))"),
        rewrite!("free-zero-id"; "(app (app free ?s) 0)" => "?s"),
        rewrite!("comm-free"; "(app (app free (app (app free ?s) ?q)) ?p)" => "(app (app free (app (app free ?s) ?p)) ?q)"),
        rewrite!("free-malloc-id"; "(letp ?s' ?p (app (app malloc ?s) ?z) (app (app free ?s') ?p))" => "?s"),
        rewrite!("comm-malloc";
            "(letp ?s0 ?p0 (app (app malloc ?s) ?z0) (letp ?s1 ?p1 (app (app malloc ?s0) ?z1) ?e)))"
            => "(letp ?s0 ?p1 (app (app malloc ?s) ?z1) (letp ?s1 ?p0 (app (app malloc ?s0) ?z0) ?e)))"
            if does_not_contain([var("?z0")], var("?z1"))
        ),
        rewrite!("malloc-loadable-ub"; "
            (letp ?s' ?p (app (app malloc ?s) ?z) (app (app free (app (app loadable ?s') ?p)) ?p))" 
            => "ub"),
        rewrite!("malloc-storable"; "
                (letp ?s' ?p (app (app malloc ?s) ?z) (app (app free (app (app storable ?s') ?p)) ?p))" 
                => "(app (app assert (app (app >= ?z) 1)) ?s)"),
        rewrite!("load-zero-ub"; "(app (app load ?s) 0)" => "ub"),
        rewrite!("load-uninitialized-ub"; "(letp ?s' ?p (app (app malloc ?s) ?z) (app (app load ?s') ?p))" => "ub"),
        rewrite!("load-emp-ub"; "(app (app load .) ?p)" => "ub"),
        rewrite!("load-free-ub"; "(app (app load (app (app free ?s) ?p)) ?p)" => "ub"),
        rewrite!("store-zero-ub"; "(app (app store ?s) 0)" => "ub"),
        rewrite!("store-emp-ub"; "(app (app store .) ?p)" => "ub"),
        rewrite!("store-free-ub"; "(app (app store (app (app free ?s) ?p)) ?p)" => "ub"),
        rewrite!("store-free-merge"; "(app (app free (app (app (app store ?s) ?p) ?v)) ?p)" => "(app (app free (app (app storable ?s) ?p)) ?p)"),
        rewrite!("store-store-merge"; "(app (app (app store (app (app (app store ?s) ?p) ?a)) ?p) ?b)" => "(app (app (app store ?s) ?p) ?b)"),
        rewrite!("load-store-merge"; "(app (app load (app (app (app store ?s) ?p) ?v)) ?p)" => "(app (app pair (app (app (app store ?s) ?p) ?v)) ?v)"),
        rewrite!("concurrent-store-ub"; "(app (app ~ (app (app (app store ?sl) ?p) ?vl)) (app (app (app store ?sr) ?p) ?vr))" => "ub"),
        rewrite!("load-pure"; 
            "(letp ?s' ?v (app (app load ?s) ?p) ?e)" 
            => "(let ?s' (app (app loadable ?s) ?p) ?e)" 
            if does_not_contain([var("?v")], var("?e"))),
        rewrite!("load-loadable-merge"; "(app (app load (app (app loadable ?s) ?p)) ?p)" => "(app (app load ?s) ?p)"),
        rewrite!("loadable-loadable-merge"; "(app (app loadable (app (app loadable ?s) ?p)) ?p)" => "(app (app loadable ?s) ?p)"),
        rewrite!("store-storable-merge"; "(app (app (app store (app (app storable ?s) ?p)) ?p) ?v)" => "(app (app (app store ?s) ?p) ?v)"),
        rewrite!("storable-store-merge"; "(app (app storable (app (app (app store ?s) ?p) ?v)) ?p)" => "(app (app (app store ?s) ?p) ?v)"),
        rewrite!("storable-storable-merge"; "(app (app storable (app (app storable ?s) ?p)) ?p)" => "(app (app storable ?s) ?p)"),
        rewrite!("loadable-comm"; "(app (app loadable (app (app loadable ?s) ?p)) ?q)" => "(app (app loadable (app (app loadable ?s) ?q)) ?p)"),
        rewrite!("storable-comm"; "(app (app storable (app (app storable ?s) ?p)) ?q)" => "(app (app storable (app (app storable ?s) ?q)) ?p)"),
        rewrite!("loadable-storable-comm"; "(app (app loadable (app (app storable ?s) ?p)) ?q)" => "(app (app storable (app (app loadable ?s) ?q)) ?p)"),
        rewrite!("storable-loadable-comm"; "(app (app storable (app (app loadable ?s) ?q)) ?p)" => "(app (app loadable (app (app storable ?s) ?p)) ?q)"),
        rewrite!("assert-storable-comm"; "(app (app assert (app (app storable ?s) ?p)) ?q)" => "(app (app storable (app (app assert ?s) ?q)) ?p)"),
        rewrite!("assert-loadable-comm"; "(app (app assert (app (app loadable ?s) ?q)) ?p)" => "(app (app loadable (app (app assert ?s) ?p)) ?q)"),
        rewrite!("loadable-assert-comm"; "(app (app loadable (app (app assert ?s) ?p)) ?q)" => "(app (app assert (app (app loadable ?s) ?q)) ?p)"),
        rewrite!("storable-assert-comm"; "(app (app storable (app (app assert ?s) ?q)) ?p)" => "(app (app assert (app (app storable ?s) ?p)) ?q)"),
        // Arithmetic
        rewrite!("comm-add"; "(app (app + ?x) ?y)" => "(app (app + ?y) ?x)"),
        rewrite!("comm-mul"; "(app (app * ?x) ?y)" => "(app (app * ?y) ?x)"),
        rewrite!("comm-and"; "(app (app & ?x) ?y)" => "(app (app & ?y) ?x)"),
        rewrite!("comm-or"; "(app (app | ?x) ?y)" => "(app (app | ?y) ?x)"),
        rewrite!("comm-xor"; "(app (app ^ ?x) ?y)" => "(app (app ^ ?y) ?x)"),
        rewrite!("add-eval"; "(app (app + ?x) ?y)" => { AddEval {
            from_pat: pat("(app (app + ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("sub-eval"; "(app (app - ?x) ?y)" => { SubEval {
            from_pat: pat("(app (app - ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("mul-eval"; "(app (app * ?x) ?y)" => { MulEval {
            from_pat: pat("(app (app * ?x) ?y)"),
            to_pat: pat("?c"),
            shl_pat: pat("(app (app << ?x) ?c)"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("div-eval"; "(app (app / ?x) ?y)" => { DivEval {
            from_pat: pat("(app (app / ?x) ?y)"),
            to_pat: pat("?c"),
            shr_pat: pat("(app (app >> ?x) ?c)"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("and-eval"; "(app (app ^ ?x) ?y)" => { AndEval {
            from_pat: pat("(app (app ^ ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("or-eval"; "(app (app | ?x) ?y)" => { OrEval {
            from_pat: pat("(app (app | ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("xor-eval"; "(app (app ^ ?x) ?y)" => { XorEval {
            from_pat: pat("(app (app ^ ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("le-eval"; "(app (app <= ?x) ?y)" => { LeEval {
            from_pat: pat("(app (app <= ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("lt-eval"; "(app (app < ?x) ?y)" => { LtEval {
            from_pat: pat("(app (app < ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("ge-eval"; "(app (app >= ?x) ?y)" => { GeEval {
            from_pat: pat("(app (app >= ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("gt-eval"; "(app (app > ?x) ?y)" => { GtEval {
            from_pat: pat("(app (app > ?x) ?y)"),
            to_pat: pat("?c"),
            a: var("?x"),
            b: var("?y"),
            c: var("?c") }
        }),
        rewrite!("add-zero"; "(app (app + ?x) 0)" => "?x"),
        rewrite!("add-self"; "(app (app + ?x) ?x)" => "(app (app * ?x) 2)"),
        rewrite!("sub-zero"; "(app (app - ?x) 0)" => "?x"),
        rewrite!("mul-one"; "(app (app * ?x) 1)" => "?x"),
        rewrite!("div-one"; "(app (app / ?x) 1)" => "?x"),
        rewrite!("or-zero"; "(app (app | ?x) 0)" => "?x"),
        rewrite!("or-false"; "(app (app | ?x) false)" => "?x"),
        rewrite!("xor-zero"; "(app (app ^ ?x) 0)" => "?x"),
        rewrite!("xor-false"; "(app (app ^ ?x) false)" => "?x"),
        rewrite!("and-one"; "(app (app & ?x) 18446744073709551615)" => "?x"),
        rewrite!("and-true"; "(app (app & ?x) true)" => "?x"),
        rewrite!("zero-sub-neg"; "(app (app - 0) ?x)" => "(app neg ?x)"),
        rewrite!("neg-zero-sub"; "(app neg ?x)" => "(app (app - 0) ?x)"),
        rewrite!("xor-one-not"; "(app (app ^ 18446744073709551615) ?x)" => "(app not ?x)"),
        rewrite!("xor-true-not"; "(app (app ^ true) ?x)" => "(app not ?x)"),
        rewrite!("not-not-id"; "(app not (app not ?x))" => "?x"),
        rewrite!("not-true"; "(app not true)" => "false"),
        rewrite!("not-false"; "(app not false)" => "true"),
        rewrite!("or-self"; "(app (app | ?x) ?x)" => "?x"),
        rewrite!("and-self"; "(app (app & ?x) ?x)" => "?x"),
        rewrite!("shl-sum"; "(app (app << (app (app << ?x) ?y)) ?z)" => "(app (app << ?x) (app (app + ?y) ?z))"),
        rewrite!("refine-mul-zero"; "(app (app * ?x) 0)" => "0" if arith_refine([var("?x")])),
        rewrite!("refine-sub-self"; "(app (app - ?x) ?x)" => "0" if arith_refine([var("?x")])),
        rewrite!("refine-xor-self"; "(app (app ^ ?x) ?x)" => "0" if arith_refine([var("?x")])),
        rewrite!("refine-and-zero"; "(app (app & 0) ?x)" => "0" if arith_refine([var("?x")])),
        rewrite!("refine-or-one"; "(app (app | 18446744073709551615) ?x)" => "18446744073709551615" if arith_refine([var("?x")])),
    ]
}

fn simplify(rules: &[Rewrite<BabyIso, IsoAnalysis>], s: &str) -> Result<(), anyhow::Error> {
    let now = Instant::now();
    let expr: RecExpr<BabyIso> = s.parse()?;
    let mut runner = Runner::default()
        .with_explanations_enabled()
        .with_expr(&expr)
        .run(rules);
    let root = runner.roots[0];
    let extractor = Extractor::new(&runner.egraph, IsoCost);
    let (best_cost, best) = extractor.find_best(root);
    let elapsed = now.elapsed();
    let original = IsoCost.cost_rec(&expr);
    println!(
        "{best} (cost = {original} => {best_cost} (ratio = {}))",
        original / best_cost
    );
    runner.print_report();
    println!("total time: {elapsed:?}");

    let mut equivalence = runner.explain_equivalence(&expr, &best);
    let mut back = false;
    let mut forwd = false;
    let mut stack = vec![equivalence.explanation_trees.iter()];
    let mut seen = FxHashSet::default();
    let mut let_steps = 0;
    while let Some(top) = stack.pop() {
        for rule in top {
            if seen.contains(&(&**rule as *const _)) {
                break;
            }
            if let Some(backward) = &rule.backward_rule {
                //println!("backwards {backward}");
                if backward.as_str().contains("refine") {
                    back = true;
                }
                if backward.as_str().contains("let") {
                    let_steps += 1;
                }
            }
            if let Some(forward) = &rule.forward_rule {
                //println!("forwards {forward}");
                if forward.as_str().contains("refine") {
                    forwd = true;
                }
                if forward.as_str().contains("let") {
                    let_steps += 1;
                }
            }
            seen.insert(&**rule as *const _);
            for proof in rule.child_proofs.iter() {
                stack.push(proof.iter())
            }
        }
    }
    println!(
        "Refinement ({} steps; {}% let): {}",
        equivalence.make_flat_explanation().len(),
        (let_steps as f64 / equivalence.make_flat_explanation().len() as f64) * 100.0,
        if forwd {
            if back {
                "VERY BAD: things have no relation"
            } else {
                "GOOD: is a valid refinement"
            }
        } else if back {
            "BAD: is refined by, rather than being a refinement"
        } else {
            "VERY GOOD: is equivalent"
        }
    );
    //println!("{}", equivalence.get_flat_string());
    Ok(())
}

fn main() {
    let mut rl = Editor::<()>::new().unwrap();
    let rules = make_rules();
    loop {
        let readline = rl.readline(">> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                if let Err(err) = simplify(&rules, line.as_str()) {
                    println!("err {err}")
                }
            }
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
}
